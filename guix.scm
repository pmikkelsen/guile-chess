(use-modules (guix packages)
	     (guix build-system gnu)
	     (guix licenses)
	     (guix gexp)
	     (gnu packages pkg-config)
	     (gnu packages guile)
	     (gnu packages autotools)
	     (gnu packages games))

(define %source-dir (string-append (dirname (current-filename))))

(package
 (name "guile-chess")
 (version "0.0.2")
 (source (local-file %source-dir #:recursive? #t))
 (build-system gnu-build-system)
 (arguments
  '(#:phases (modify-phases %standard-phases
                (add-before 'configure 'autoreconf
		   (lambda _
		     (zero? (system* "autoreconf" "-vfi")))))))
 (native-inputs
  `(("autoconf" ,autoconf)
    ("automake" ,automake)
    ("pkg-config" ,pkg-config)))
 (propagated-inputs `(("chess" ,chess)
		      ("guile" ,guile-2.0)))
 (synopsis "UCI chess wrapper in guile scheme")
 (description "A pure scheme uci chess engine wrapper")
 (home-page "https://alaok.org/link-sometime-in-future")
 (license gpl3+))
