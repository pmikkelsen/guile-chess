(define-module (chess uci-engine)
  #:use-module (ice-9 rdelim)
  #:use-module (chess data)
  #:use-module (chess gameplay)
  #:export (game-board
	    new-game
	    computer-move
	    position-string
	    run-engine
	    move))

(define input #f)
(define output #f)

(define position-string "position startpos moves")
(define game-board #f)

(define (run-engine chess-engine)
  (let* ((to-engine (pipe))
	 (from-engine (pipe))
	 (engine-read (car to-engine))
	 (engine-write (cdr from-engine))
	 (pid (primitive-fork)))
    (set! input (car from-engine))
    (set! output (cdr to-engine))
    (when (= pid 0)
	  (dup2 (fileno engine-read) 0)
	  (dup2 (fileno engine-write) 1)
	  (execlp chess-engine))))

(define (send-command cmd)
  (display (string-append cmd "\n") output)
  (force-output output))

(define (wait-for-reply reply)
  (if (char-ready? input)
      (let ((input-from-engine (read-line input)))
	(if (string-prefix? reply input-from-engine)
	    input-from-engine
	    (wait-for-reply reply)))
      (begin
	(usleep 100)
	(wait-for-reply reply))))

(define (send-command-with-reply cmd reply)
  (send-command cmd)
  (wait-for-reply reply))

;; game functions
(define (set-position)
  (send-command position-string))

(define (new-game start-board)
  (set! position-string "position startpos moves")
  (set! game-board start-board)
  (send-command-with-reply "uci" "uciok")
  (send-command-with-reply "isready" "readyok")
  (send-command "ucinewgame")
  (set-position))

(define (move move-str)
  (let ((type (legal-move? (string->move move-str) game-board)))
    (if type
	(begin
	  (set! position-string (string-append position-string " " move-str))
	  (set-position)
	  (set! game-board (modify-board game-board (string->move move-str) type))
	  (string->move move-str))
	#f)))

(define (computer-move)
  (let* ((bestmove (send-command-with-reply "go movetime 500"  "bestmove"))
	 (move-str (substring bestmove 9 13)))
    (move move-str)))
