(define-module (chess helper)
  #:use-module (chess data)
  #:use-module (srfi srfi-1)
  #:export (vertical-moves
	    horizontal-moves
	    diagonal-moves
	    xy-add
	    y-add
	    x-add
	    free-path))


;;------------------------------------------------------------------------------
;; Util functions. Should probs be somewhere else
;;------------------------------------------------------------------------------
(define* (vertical-moves position #:key (distance 7))
  (map (lambda (y)
	 (y-add position y))
       (append (iota distance -1 -1)
	       (iota distance 1 1))))

(define* (horizontal-moves position #:key (distance 7))
  (map (lambda (x)
	 (x-add position x))
       (append (iota distance -1 -1)
	       (iota distance 1 1))))

(define* (xy-iota n start inc-x inc-y #:key (acc '()))
  (if (not start)
      (filter (lambda (x) x) acc)
      (let ((new (xy-add start inc-x inc-y)))
	(if (= n (length acc))
	    acc
	    (xy-iota n new inc-x inc-y #:acc (cons new acc))))))

(define* (diagonal-moves position #:key (distance 7))
  (append (xy-iota distance position 1 1)
	  (xy-iota distance position 1 -1)
	  (xy-iota distance position -1 1)
	  (xy-iota distance position -1 -1)))

(define (all-true list)
  (every (lambda (x) x) list))

(define (sign x)
  (if (negative? x)
      -1
      1))

(define (free-path-vertical board from to)
  (let* ((distance (- (y-pos to) (y-pos from)))
	 (step (sign distance)))
    (all-true (map (lambda (y)
		     (is-free? (y-add from y) board))
		   (iota (- (abs distance) 1) step step)))))

(define (free-path-horizontal board from to)
  (let* ((distance (- (x-pos-int to) (x-pos-int from)))
	 (step (sign distance)))
    (all-true (map (lambda (x)
		     (is-free? (x-add from x) board))
		   (iota (- (abs distance) 1) step step)))))

(define (free-path-diagonal board from to)
  (let* ((distance-x (- (x-pos-int to) (x-pos-int from)))
	 (distance-y (- (y-pos to) (y-pos from)))
	 (step-x (sign distance-x))
	 (step-y (sign distance-y)))
    (all-true (map (lambda (x y)
		     (not (piece-at-pos (xy-add from x y) board)))
		   (iota (- (abs distance-x) 1) step-x step-x)
		   (iota (- (abs distance-y) 1) step-y step-y)))))

(define (free-path from to board)
  (let ((x-from (x-pos-int from))
	(x-to (x-pos-int to))
	(y-from (y-pos from))
	(y-to (y-pos to)))
    (cond
     ((= x-from x-to) (free-path-vertical board from to))
     ((= y-from y-to) (free-path-horizontal board from to))
     ((= (abs (- y-from y-to))
	 (abs (- x-from x-to)))
      (free-path-diagonal board from to))
     (else #f))))

(define (next-x current)
  (case current
    ((a) 'b)
    ((b) 'c)
    ((c) 'd)
    ((d) 'e)
    ((e) 'f)
    ((f) 'g)
    ((g) 'h)
    (else #f)))

(define (prev-x current)
  (case current
    ((b) 'a)
    ((c) 'b)
    ((d) 'c)
    ((e) 'd)
    ((f) 'e)
    ((g) 'f)
    ((h) 'g)
    (else #f)))

(define (add-to-x current n)
  (cond
   ((and (> n 0))
    (add-to-x (next-x current) (- n 1)))
   ((and (< n 0))
    (add-to-x (prev-x current) (+ n 1)))
   (else current)))

(define (x-add position n)
  (xy->position (add-to-x (x-pos position) n)
		(y-pos position)))

(define (y-add position n)
  (xy->position (x-pos position)
		(cond
		 ((> (+ (y-pos position) n) 8) #f)
		 ((< (+ (y-pos position) n) 1) #f)
		 (else (+ n (y-pos position))))))

(define (xy-add position nx ny)
  (let ((x (x-add position nx))
	(y (y-add position ny)))
    (if (and x y)
	(xy->position (x-pos (x-add position nx))
		      (y-pos (y-add position ny)))
	#f)))
