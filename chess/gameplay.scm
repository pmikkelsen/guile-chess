(define-module (chess gameplay)
  #:use-module (srfi srfi-1)
  #:use-module (chess data)
  #:use-module (chess helper)
  #:export (legal-move?
	    modify-board))

;;------------------------------------------------------------------------------
;; Normal move rules and helper functions
;;------------------------------------------------------------------------------

;; a list of positions a king can move to from the given position
(define (valid-king-moves position)
  (append (vertical-moves position #:distance 1)
	  (horizontal-moves position #:distance 1)
	  (diagonal-moves position #:distance 1)))

;; a list of positions a queen can move to from the given position
(define (valid-queen-moves position)
  (append (vertical-moves position)
	  (horizontal-moves position)
	  (diagonal-moves position)))

;; a list of positions a bishop can move to from the given position
(define (valid-bishop-moves position)
  (diagonal-moves position))

;; a list of positions a knight can move to from the given position
(define (valid-knight-moves position)
  (list (xy-add position 1 2)
	(xy-add position -1 2)
	(xy-add position 2 1)
	(xy-add position -2 1)
	(xy-add position 1 -2)
	(xy-add position -1 -2)
	(xy-add position 2 -1)
	(xy-add position -2 -1)))

;; a list of positions a rook can move to from the given position
(define (valid-rook-moves position)
  (append (horizontal-moves position)
	  (vertical-moves position)))

;; a list of positions a pawn can move to from the given position
(define (valid-pawn-moves position color capture)
  (if capture
      (case color
	((white) (list (xy-add position 1 1)
		       (xy-add position -1 1)))
	((black) (list (xy-add position 1 -1)
		       (xy-add position -1 -1))))
      (cond
       ((and (eq? color 'black) (= 7 (y-pos position)))
	(list (y-add position -1)
	      (y-add position -2)))
       ((eq? color 'black)
	(list (y-add position -1)))
       ((and (eq? color 'white) (= 2 (y-pos position)))
	(list (y-add position 1)
	      (y-add position 2)))
       ((eq? color 'white)
	(list (y-add position 1)))
       (else '()))))

;; a list of positions a given piece can get to given its position.
(define (valid-normal-moves piece position capture)
  (let ((color (piece-color piece))
	(type (piece-type piece)))
    (case type
      ((king) (valid-king-moves position))
      ((queen) (valid-queen-moves position))
      ((bishop) (valid-bishop-moves position))
      ((knight) (valid-knight-moves position))
      ((rook) (valid-rook-moves position))
      ((pawn) (valid-pawn-moves position color capture))
      (else '()))))

;;------------------------------------------------------------------------------
;; Castling rules and helper functions
;;------------------------------------------------------------------------------

;; When a castling move is performed, how should the rook move
(define (castling-rook-move move)
  (cond
   ((equal? move (string->move "e1g1")) (string->move "h1f1")) ; white kingside
   ((equal? move (string->move "e8g8")) (string->move "h8f8")) ; black kingside
   ((equal? move (string->move "e1c1")) (string->move "a1d1")) ; white queenside
   ((equal? move (string->move "e8c8")) (string->move "a8d8")) ; black queenside
   (else #f)))

;; Is the move a castling move, and is it possible on the given board?
(define (valid-castling? move board)
  (let* ((from (move-from move))
	 (to (move-to move))
	 (rook-pos (if (castling-rook-move move)
		       (move-from (castling-rook-move move))
		       #f)))
    (and rook-pos
	 (free-path from rook-pos board)
	 (= 0
	    (piece-at-pos-moves from board)
	    (piece-at-pos-moves rook-pos board)))))


;;------------------------------------------------------------------------------
;; En passant rules and helper functions
;;------------------------------------------------------------------------------

;; When move is a valid en passant attack, where is the target?
(define (en-passant-target-pos move)
  (let ((x (x-pos (move-to move)))
	(y (y-pos (move-from move))))
    (xy->position x y)))

;; Is the move an en passant move, and is it possible on the given board?
(define (valid-en-passant? move board)
  (let ((from (move-from move))
	(to (move-to move))
	(target (piece-at-pos (en-passant-target-pos move) board))
	(target-moves (piece-at-pos-moves (en-passant-target-pos move) board))
	(current-move (board-n-moves board))
	(target-last-move (piece-at-pos-last-move (en-passant-target-pos move) board)))
    (and (member to (diagonal-moves from #:distance 1))
	 target
	 (is-free? to board)
	 (eq? (piece-type target) 'pawn) ; target is a pawn
	 (eq? target-moves 1) ; target has just moved from its start position
	 (= (- current-move target-last-move) 1) ; target moved in last round
	 )))


;;------------------------------------------------------------------------------
;; Check and check mate
;;------------------------------------------------------------------------------

(define (check? color board)
  (let* ((positions (board-pieces board))
	 (pieces (map (lambda (pair) (cons (board-piece (cdr pair)) (car pair))) positions))
	 (opponent-color (if (eq? color 'white) 'black 'white))
	 (king-pos (cdr (assoc (color+type->piece color 'king) pieces)))
	 (opponents (map (lambda (pair)
			      (let ((c (piece-color (car pair))))
				(if (eq? c opponent-color)
				    (cdr pair)
				    king-pos)))
			    pieces))
	 (moves (map (lambda (p) (make-move p king-pos)) opponents)))
    (any (lambda (m) (legal-move? m board #:checking-for-check #t)) moves)))

; Is the given move possible on the board. If yes, what type of move is it.
(define (valid-move? move board)
  (let* ((from (move-from move))
	 (to (move-to move))
	 (player-piece (piece-at-pos from board))
	 (player-color (if player-piece (piece-color player-piece) #f))
	 (target-piece (piece-at-pos to board))
	 (target-color (if target-piece (piece-color target-piece) #f))
	 (capture (piece-at-pos to board)))
    (cond
     ((not player-piece) #f)
     ((eq? from to) #f)
     ((eq? player-color target-color) #f)
     ((member to (valid-normal-moves player-piece from capture)) 'normal)
     ((valid-castling? move board) 'castling)
     ((valid-en-passant? move board) 'en-passant)
     (else #f))))

;;------------------------------------------------------------------------------
;; Global move checker function. Is the move given a legal move on the given
;; board? (Valid move + free path)
;;------------------------------------------------------------------------------
(define* (legal-move? move board #:key (checking-for-check #f))
  (when (move? move)
	(let* ((from (move-from move))
	       (to (move-to move))
	       (piece (piece-at-pos from board))
	       (move-type (and (not (equal? from to))
			       piece
			       (if (eq? (piece-type piece) 'knight)
				   #t
				   (free-path from to board))
			       (valid-move? move board))))
	  (if (or checking-for-check
		  (and move-type
		       (not (check? (piece-color piece) (modify-board board move move-type)))))
	      move-type
	      #f))))

;;------------------------------------------------------------------------------
;; Function to modify the board according to the rules
;;------------------------------------------------------------------------------

(define (modify-board board move move-type)
  (let ((board-n (board-n-moves board))
	(board-list (board-pieces board)))
    (when (member move-type '(normal en-passant castling)) ;normal, en passant, castling
      (let* ((from (move-from move))
	     (to (move-to move))
	     (piece (piece-at-pos from board))
	     (n-moves (piece-at-pos-moves from board))
	     (new (make-board-piece piece (1+ n-moves) board-n)))
	(set! board-list (alist-delete from board-list))
	(set! board-list (alist-delete to board-list))
	(set! board-list (acons to new board-list))))
    (when (eq? move-type 'castling) ; only castling
      (let* ((rook-pos (move-from (castling-rook-move move)))
	     (rook (piece-at-pos rook-pos board))
	     (new-rook-pos (move-to (castling-rook-move move)))
	     (new-rook (make-board-piece rook 1 board-n))) ; castling. This is the rooks first move
	(set! board-list (alist-delete rook-pos board-list))
	(set! board-list (acons new-rook-pos new-rook board-list))))
    (when (eq? move-type 'en-passant) ; only en passant
      (let ((target-pos (en-passant-target-pos move)))
	(set! board-list (alist-delete target-pos board-list))))
    (make-board board-list (1+ board-n))))
