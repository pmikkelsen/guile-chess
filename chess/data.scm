(define-module (chess data)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-1)
  #:export (
	    ; board pieces
	    make-board-piece
	    board-piece
	    
	    ; board
	    board->string
	    is-free?
	    make-board
	    piece-at-pos
	    board-n-moves
	    board-pieces
	    piece-at-pos-moves
	    piece-at-pos-last-move

	    ; move
	    move?
	    string->move
	    move-from
	    move-to
	    make-move

	    ; piece
	    piece->char
	    piece-color
	    piece-type
	    color+type->piece

	    ; position
	    y-pos
	    x-pos
	    x-pos-int
	    xy->position
	    string->position))

;;------------------------------------------------------------------------------
;; A PIECE ON A BOARD
;;------------------------------------------------------------------------------
(define-record-type <board-piece>
  (make-board-piece piece n-moves last-move)
  board-piece?
  (piece board-piece)
  (n-moves board-piece-n-moves)
  (last-move board-piece-last-move))

;;------------------------------------------------------------------------------
;; A BOARD
;;------------------------------------------------------------------------------
(define-record-type <board>
  (make-board board-pieces-list move-count)
  board?
  (board-pieces-list board-pieces)
  (move-count board-n-moves))

; assoc function for a board
(define (board-assoc pos board)
  (let ((lookup-res (assoc pos (board-pieces board))))
    (if lookup-res
	(cdr lookup-res)
	#f)))

; return the piece at the given position
(define (piece-at-pos pos board)
  (let ((lookup-res (board-assoc pos board)))
    (if (board-piece? lookup-res)
	(board-piece lookup-res)
	#f)))

; return the number of moves a piece at a given position has made
(define (piece-at-pos-moves pos board)
  (let ((lookup-res (board-assoc pos board)))
    (if (board-piece? lookup-res)
	(board-piece-n-moves lookup-res)
	#f)))

; return the last round the piece at a given position moved
(define (piece-at-pos-last-move pos board)
  (let ((lookup-res (board-assoc pos board)))
    (if (board-piece? lookup-res)
	(board-piece-last-move lookup-res)
	#f)))

; is the given position free?
(define (is-free? pos board)
  (not (piece-at-pos pos board)))

; show a row on the board as a string applying *func* to all positions
(define (row->string board row-nr func)
  (apply string-append
	 (map
	  (lambda (x-pos)
	    (define pos (xy->position x-pos row-nr))
	    (format #f "~a " (let ((c (func pos board)))
			       (if c
				   c
				   #\space))))
	  '(a b c d e f g h))))

; show the entire board applying *func* to all positions
(define (board->string board func)
  (apply string-append
	 (map (lambda (y-pos)
		(format #f "~a~%" (row->string board y-pos func)))
	      (iota 8 8 -1))))

;;------------------------------------------------------------------------------
;; A POSITION
;;------------------------------------------------------------------------------
(define-record-type <position>
  (make-position x y)
  position?
  (x x-pos)
  (y y-pos))

; custom printer for the position type. Print as xy
(set-record-type-printer!
 <position>
 (lambda (record port)
   (display (x-pos record) port)
   (display (y-pos record) port)))

; make a position from x and y coordinates
(define (xy->position x y)
  (if (and (valid-x? x)
	   (valid-y? y))
      (make-position x y)
      #f))

; make a position from a string
(define (string->position str)
  (let ((x (string->symbol (substring str 0 1)))
	(y (string->number (substring str 1 2))))
    (xy->position x y)))

; valid x position?
(define (valid-x? x)
  (and (symbol? x) (member x '(a b c d e f g h)) #t))

; valid y position?
(define (valid-y? y)
  (and (integer? y) (> y 0) (< y 9)))

; return the x position as an int
(define (x-pos-int pos)
  (case (x-pos pos)
    ((a) 1)
    ((b) 2)
    ((c) 3)
    ((d) 4)
    ((e) 5)
    ((f) 6)
    ((g) 7)
    ((h) 8)))

;;------------------------------------------------------------------------------
;; A MOVE
;;------------------------------------------------------------------------------
(define-record-type <move>
  (make-move from to)
  move?
  (from move-from)
  (to move-to))

; custom printer for the move type. Print as from->to
(set-record-type-printer!
 <move>
 (lambda (record port)
   (display (move-from record) port)
   (display "->" port)
   (display (move-to record) port)))

; make a move from two positions
(define (positions->move from to)
  (if (and (position? from)
	   (position? to))
      (make-move from to)
      #f))

; turn a string into a move
(define (string->move str)
  (if (= (string-length str) 4)
      (let ((from (string->position (substring str 0 2)))
	    (to (string->position (substring str 2 4))))
	(positions->move from to))
      #f))

;;------------------------------------------------------------------------------
;; A PIECE
;;------------------------------------------------------------------------------
(define-record-type <piece>
  (make-piece color type)
  piece?
  (color piece-color)
  (type piece-type))

; custom printer for the piece type. Prints as "color type"
(set-record-type-printer!
 <piece>
 (lambda (record port)
   (display (piece-color record) port)
   (display " " port)
   (display (piece-type record) port)))

; constructor for the piece type
(define (color+type->piece color type)
  (if (and (piece-color? color)
	   (piece-type? type))
      (make-piece color type)
      #f))

; check for valid colors
(define (piece-color? color)
  (member color '(black white)))

; check for valid piece-types
(define (piece-type? type)
  (member type '(pawn rook knight bishop king queen)))

; convert a piece to a char for display
(define (piece->char piece)
  (if (piece? piece)
      (let ((ch (case (piece-type piece)
		  ((rook) #\r)
		  ((knight) #\n)
		  ((bishop) #\b)
		  ((queen) #\q)
		  ((king) #\k)
		  ((pawn) #\p))))
	(if (eq? 'white (piece-color piece))
	    (char-upcase ch)
	    ch))
      #f))
