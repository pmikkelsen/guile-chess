(use-modules (chess data)
	     (chess uci-engine)
	     (chess helper)
	     (chess gameplay)
	     (gpio)
	     (gpio lcd)
	     (gpio motor-driver)
	     (ice-9 readline)
	     (ice-9 rdelim)
	     (srfi srfi-1))

;; Configuration

(define i2c-bus 1) ; Screen is on i2c bus 1 on the raspberry pi
(define motor1-dir-pin 20) ; Direction pin on motor 1
(define motor2-dir-pin 19) ; Direction pin on motor 2
(define motor1-step-pin 21) ; Step pin on motor 1
(define motor2-step-pin 26) ; Step pin on motor 2

(define motor-1 (make-motor-driver motor1-dir-pin motor1-step-pin 8 200)) ; 1/16 steps, 200 full steps per round
(define motor-2 (make-motor-driver motor2-dir-pin motor2-step-pin 10 200)) ; 10/steps, 200 full steps per round

(define grid-size (* 4 20)) ; how many rounds is there in one square

(define %configs `((pin ,motor1-dir-pin output low)
		   (pin ,motor2-dir-pin output low)
		   (pin ,motor1-step-pin output low)
		   (pin ,motor2-step-pin output low)
		   (pin 16 output low)))
;; Configuration end

;; Global variables
(define motor-positions '(#f . #f))
;; Global variables end

(define start-board
  (make-board (map (lambda (list)
		     (let* ((pos (string->position (car list)))
			    (piece (color+type->piece (cadr list) (caddr list))))
		       (cons pos (make-board-piece piece 0 0))))
		   ;; white pieces
		   '(("a1" white rook)
		     ("a1" white rook)
		     ("b1" white knight)
		     ("c1" white bishop)
		     ("d1" white queen)
		     ("e1" white king)
		     ("f1" white bishop)
		     ("g1" white knight)
		     ("h1" white rook)
		     ("a2" white pawn)
		     ("b2" white pawn)
		     ("c2" white pawn)
		     ("d2" white pawn)
		     ("e2" white pawn)
		     ("f2" white pawn)
		     ("g2" white pawn)
		     ("h2" white pawn)
		     ;; black pieces
		     ("a7" black pawn)
		     ("b7" black pawn)
		     ("c7" black pawn)
		     ("d7" black pawn)
		     ("e7" black pawn)
		     ("f7" black pawn)
		     ("g7" black pawn)
		     ("h7" black pawn)
		     ("a8" black rook)
		     ("b8" black knight)
		     ("c8" black bishop)
		     ("d8" black queen)
		     ("e8" black king)
		     ("f8" black bishop)
		     ("g8" black knight)
		     ("h8" black rook)
		     ))
	      0))

(define (take-safe list n)
  (let ((list-length (length list)))
    (if (>= list-length n)
	(take list n)
	list)))

(define (drop-safe list n)
  (let ((list-length (length list)))
    (if (>= list-length n)
	(drop list n)
	'())))

(define (get-input prompt1 prompt2 acc cx1 cy1 cx2 cy2)
  (let* ((from (list->string (take-safe acc 2)))
	 (to (list->string (take-safe (drop-safe acc 2) 2)))
	 (check (case (length acc)
		  ((0) cx1)
		  ((1) (lambda (y) (cy1 (first acc) y)))
		  ((2) (lambda (x) (cx2 from x)))
		  ((3) (lambda (y) (cy2 from (third acc) y)))
		  (else (lambda (x) #f)))))
    (system "stty raw")
    (clear)
    (lcd-string (string-append prompt1 from))
    (line-2)
    (lcd-string (string-append prompt2 to))
    (let ((c (read-char)))
      (cond
       ((equal? c #\q) (system "stty sane") #f)
       ((and (equal? c #\delete) (= (length acc) 0))
	(get-input prompt1 prompt2 acc cx1 cy1 cx2 cy2))
       ((equal? c #\delete)
	(get-input prompt1 prompt2 (drop-right acc 1) cx1 cy1 cx2 cy2))
       ((and (equal? c #\return) (= (length acc) 4))
	(system "stty sane")
	(list->string acc))
       ((check c)
	(lcd-color 0 255 0)
	(get-input prompt1 prompt2 (reverse (cons c (reverse acc))) cx1 cy1 cx2 cy2))
       (else
	(lcd-color 255 100 0)
	(usleep 500000)
	(lcd-color 0 255 0)
	(get-input prompt1 prompt2 acc cx1 cy1 cx2 cy2))))))

(define (get-move prompt1 prompt2)
  (get-input prompt1      prompt2
	     '()
	     valid-x-from valid-y-from
	     valid-x-to   valid-y-to))

(define (valid-x-from x)
  (and (member x (string->list "abcdefgh"))
       (any (lambda (y) (valid-y-from x y))
	    (string->list "12345678"))))

(define (valid-y-from x y)
  (let ((p (piece-at-pos (string->position (list->string (list x y))) game-board)))
    (and p (eq? 'white (piece-color p)))))

(define (valid-x-to from x)
  (and (member x (string->list "abcdefgh"))
       (any (lambda (y) (valid-y-to from x y))
	    (string->list "12345678"))))			;

(define (valid-y-to from x y)
  (and (member y (string->list "12345678"))
       (legal-move? (string->move (string-append from (list->string (list x y)))) game-board)))

(define (calculate-direction delta)
  (cond
   ((> delta 0) 'clockwise)
   ((< delta 0) 'anticlockwise)
   (else #f)))

(define (move-motors move)
  (let* ((motor1-pos (car motor-positions))
	 (motor2-pos (cdr motor-positions))
	 (delta-x (#f)) ; TODO
	 (delta-y (#f)) ; TODO
	 (direction-x (calculate-direction delta-x))
	 (direction-y (calculate-direction delta-y)))
    (cond
     ((= delta-x 0) (turn motor-2 direction-y (* delta-y grid-size)))
     ((= delta-y 0) (turn motor-1 direction-x (* delta-x grid-size)))
     (else (turn-both motor-1 motor-2
		      direction-x direction-y
		      (* delta-x grid-size) (* delta-y grid-size))))
    (set! motor-positions (cons (+ delta-x motor1-pos)
				(+ delta-y motor2-pos)))))

(define (play-lcd-game)
  (clear)
  (let* ((player-move (get-move "Select piece "
				"Where to m8? ")))
    (when player-move
	  (move-motors (move player-move))
	  (move-motors (computer-move))
	  (play-lcd-game))))

(define (calibrate)
  (system "stty raw")
  (let ((c (read-char)))
    (cond
     ((equal? c #\w)
      (turn motor-2 'clockwise 5)
      (calibrate))
     ((equal? c #\s)
      (turn motor-2 'anticlockwise 5)
      (calibrate))
     ((equal? c #\a)
      (turn motor-1 'anticlockwise 5)
      (calibrate))
     ((equal? c #\d)
      (turn motor-1 'clockwise 5)
      (calibrate))
     ((equal? c #\r)
      (turn-both motor-1 'anticlockwise 5
		 motor-2 'clockwise 5)
      (calibrate))
     ((equal? c #\m)
      (set-value 16 'high)
      (calibrate))
     ((equal? c #\o)
      (set-value 16 'low)
      (calibrate))
     ((equal? c #\return)
      (set! motor-positions (cons 0 0)))
     (else (calibrate)))))

(with-gpio %configs
	   (calibrate)
	   (set-value 16 'high)
	   (usleep 1000000)
	   (set-value 16 'low)
	   (run-engine "gnuchessu")
	   (new-game start-board)
	   (init-screen i2c-bus)
	   (play-lcd-game))
