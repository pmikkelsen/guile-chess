(define-module (gpio lcd)
  #:export (init-screen
	    clear
	    lcd-string
	    lcd-color
	    lcd-char
	    line-2))

(define lcd-bus #f)
(define current-line 1)
(define current-row 1)

(define backlight-address "0x62")
(define character-address "0x3e")

;; backlight registers
(define mode1-reg "0x00")
(define mode2-reg "0x01")
(define blue-reg "0x02")
(define green-reg "0x03")
(define red-reg "0x04")
(define ledout "0x08")

;; character registers
(define display-reg "0x80")
(define letters-reg "0x40")

(define (i2cset chip-address data-address value)
  (system* "i2cset"
	   "-y"
	   (number->string lcd-bus)
	   chip-address data-address
	   (number->string value)))

(define (set-backlight reg val)
  (i2cset backlight-address reg val))

(define (set-character reg val)
  (i2cset character-address reg val))

(define (init-screen bus)
  (set! lcd-bus bus)
  (set-backlight mode1-reg 0)
  (set-backlight mode2-reg 0)
  (lcd-color 255 255 255)
  (set-backlight ledout 170)
  (sleep 1))

(define (clear)
  (set! current-line 1)
  (set! current-row 1)
  (set-character display-reg 1)
  (set-character display-reg 15)
  (set-character display-reg 56))

(define (line-2)
  (set! current-line 2)
  (set! current-row 1)
  (set-character display-reg 192))

(define (lcd-char c)
  (cond
   ((eq? c #\newline) (line-2))
   ((= current-row 17) (line-2) (lcd-char c))
   (else (set! current-row (+ current-row 1))
	 (set-character letters-reg (char->integer c)))))

(define (lcd-string string)
  (map lcd-char (string->list string)))

(define (lcd-color red green blue)
  (set-backlight red-reg red)
  (set-backlight green-reg green)
  (set-backlight blue-reg blue))
