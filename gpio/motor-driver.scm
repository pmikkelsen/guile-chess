
(define-module (gpio motor-driver)
  #:use-module (gpio)
  #:use-module (srfi srfi-1)
  #:export (make-motor-driver
	    turn-both
	    turn))

(define (make-motor-driver dir pul res spr)
  (list dir pul res spr)) ; format (direction-pin pulse-pin resolution motor-steps-per-round)

(define (step pulse-pin n)
  (unless (= n 0)
	  (set-value pulse-pin 'high)
	  (usleep 1)
	  (set-value pulse-pin 'low)
	  (usleep 1)
	  (step pulse-pin (- n 1))))

(define (step-both pulse-pin1 pulse-pin2 n1 n2)
  (when (or (> n1 0)
	    (> n2 0))
    (unless (<= n1 0)
      (step pulse-pin1 1))
    (unless (<= n2 0)
      (step pulse-pin2 1))
    (step-both pulse-pin1 pulse-pin2 (- n1 1) (- n2 1))))

(define (turn motor direction n)
  (let* ((pulse-pin (second motor))
	 (steps-per-round (fourth motor))
	 (resolution (third motor))
	 (steps (* n steps-per-round resolution)))
    (set-direction motor direction)
    (step pulse-pin steps)))

(define (turn-both motor1 direction1 n1
		   motor2 direction2 n2)
  (let* ((pulse-pin1 (second motor1))
	 (pulse-pin2 (second motor2))
	 (steps-per-round1 (fourth motor1))
	 (steps-per-round2 (fourth motor2))
	 (resolution1 (third motor1))
	 (resolution2 (third motor2))
	 (steps1 (* n1 steps-per-round1 resolution1))
	 (steps2 (* n2 steps-per-round2 resolution2)))
    (set-direction motor1 direction1)
    (set-direction motor2 direction2)
    (step-both pulse-pin1 pulse-pin2 steps1 steps2)))

(define (set-direction motor dir)
  (let ((direction-pin (first motor)))
    (case dir
      ((clockwise) (set-value direction-pin 'high))
      ((anticlockwise) (set-value direction-pin 'low))
      (else #f))))
