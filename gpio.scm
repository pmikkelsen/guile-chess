(define-module (gpio)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 rdelim)
  #:export (with-gpio
	    set-value
	    get-value
	    export-pin
;	    read-pin
	    ))

(define %exported-pins '())
(define %exported-value-files '())
(define %exported-direction-files '())

(define-syntax-rule (with-gpio configs action ...)
  (begin (map apply-config configs)
	 (begin action ...)
	 (unexport-pins)))

(define (apply-config config)
  (let ((pin-number (second config))
	(direction (third config)))
    (export-pin pin-number direction)
    (when (eq? 'output direction)
      (set-value pin-number (fourth config)))))

(define (unexport-pins)
  (map (lambda (pin)
	 (let ((unexport-file (open-file "/sys/class/gpio/unexport" "w")))
	   (display (car pin) unexport-file)
	   (force-output unexport-file)))
       %exported-pins))

(define (export-pin pin direction)
  (let ((export-file (open-file "/sys/class/gpio/export" "w")))
    (display pin export-file)
    (force-output export-file)
    (set! %exported-value-files
	  (acons pin
		 (open-file (pin-file-path pin "value")
			    "w")
		 %exported-value-files))
    (set! %exported-direction-files
	  (acons pin
		 (open-file (pin-file-path pin "direction")
			    "w")
		 %exported-direction-files))
    (case direction
      ((output) (write-pin pin 'direction 'out))
      ((input) (write-pin pin 'direction 'in)))
    (set! %exported-pins (acons pin direction %exported-pins))))

(define (pin-file-path number file)
  (format #f "/sys/class/gpio/gpio~a/~a" number file))

(define (write-pin number file-type symbol)
  (let ((file (cdr (assoc number (case file-type
				   ((direction) %exported-direction-files)
				   ((value) %exported-value-files))))))
    (display symbol file)
    (force-output file)))

;(define (read-pin number file)
;  (let ((file (open-file (pin-file-path number file) "r")))
;    (read-line file)))

(define (set-value pin state)
  (case state
    ((high) (write-pin pin 'value 1))
    ((low) (write-pin pin 'value 0))))

(define (get-value pin)
  (read-pin pin 'value))

